package com.jk4life.android.ichooseforyou.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.jk4life.android.ichooseforyou.R;
import com.jk4life.android.ichooseforyou.SwipeListAdapter;
import com.jk4life.android.ichooseforyou.model.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainActivity extends ActionBarActivity {

    private Context context = null;
    private SwipeListAdapter listAdapter;
    private List<Item> listItems = new ArrayList<Item>();
    private EditText editText;
    private Button submitButton;
    private Button chooseButton;
    private Button clearButton;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        // edit text
        editText = (EditText)findViewById(R.id.edit_message);
        // list view
        listView = (ListView)findViewById(R.id.item_list);
        listAdapter = new SwipeListAdapter(context, listItems);
        listView.setAdapter(listAdapter);

        // submit button
        submitButton = (Button)findViewById(R.id.button_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String itemString = editText.getText().toString();
                Item item = new Item(itemString);
                editText.setText("");
                if(itemString.length()>0) {
                    listItems.add(item);
                    listAdapter.notifyDataSetChanged();
                }
            }
        });

        // chooseButton
        chooseButton = (Button)findViewById(R.id.button_choose);
        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseItem(view);
            }
        });

        clearButton = (Button)findViewById(R.id.button_clear);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listItems.clear();
                listAdapter.notifyDataSetChanged();
            }
        });
    }

    public void chooseItem(final View view) {
        if(listItems.size() > 0) {
            Random random = new Random();
            String chosenItem = listItems.get(random.nextInt(listItems.size())).getContent();
            new AlertDialog.Builder(context)
                .setMessage(String.format("You have chosen %s\n Wanna chose Again?", chosenItem))
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        chooseItem(view);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
        } else {
            new AlertDialog.Builder(context)
            .setMessage("No items here to choose from!")
            .setCancelable(true)
            .create().show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
