package com.jk4life.android.ichooseforyou;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.jk4life.android.ichooseforyou.model.Item;

import java.util.List;

/**
 * Created by jians on 1/22/15.
 */
public class SwipeListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<Item> list;

    public SwipeListAdapter(Context context, List<Item> list) {
        inflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view;
        ViewHolder viewHolder;
        Item item = list.get(position);
        if(convertView == null) {
            view = inflater.inflate(R.layout.list_row, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.deleteButton = (Button)view.findViewById(R.id.button_delete);
            viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list.remove(position);
                    notifyDataSetChanged();
                }
            });
            viewHolder.textView = (TextView)view.findViewById(R.id.row_text);
            view.setTag(R.layout.list_row, viewHolder);
            view.setTag(R.string.list_item_key, item);
            view.setOnTouchListener(new SwipeListener(view, this));
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag(R.layout.list_row);
        }
        viewHolder.textView.setText(item.getContent());
        if(item.isVisible()) {
            viewHolder.deleteButton.setVisibility(View.VISIBLE);
        } else {
            viewHolder.deleteButton.setVisibility(View.GONE);
        }
        return view;
    }

    public class ViewHolder {
        public Button deleteButton;
        public TextView textView;
    }
}
