package com.jk4life.android.ichooseforyou;

import android.view.MotionEvent;
import android.view.View;

import com.jk4life.android.ichooseforyou.model.Item;

/**
 * Created by jians on 1/22/15.
 */
public class SwipeListener implements View.OnTouchListener {

    private float startX = 0;
    private float endX = 0;
    private float difference = 0;
    private SwipeListAdapter adapter = null;
    private SwipeListAdapter.ViewHolder viewHolder = null;
    private Item item = null;

    public SwipeListener(View view, SwipeListAdapter adapter) {
        this.adapter = adapter;
        this.viewHolder = (SwipeListAdapter.ViewHolder)view.getTag(R.layout.list_row);
        this.item = (Item)view.getTag(R.string.list_item_key);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        switch(action) {
            case MotionEvent.ACTION_DOWN:
                startX = motionEvent.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                endX = motionEvent.getX();
                difference = endX - startX;
                break;
            case MotionEvent.ACTION_UP:
                checkDifference();
                startX = endX = difference = 0;
                break;
        }
        return true;
    }

    private void checkDifference() {
        if(difference >= 70) {
            item.setVisible(true);
            viewHolder.deleteButton.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        }
        else if(difference <= -70) {
            item.setVisible(false);
            viewHolder.deleteButton.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
        }
    }
}
