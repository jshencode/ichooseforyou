package com.jk4life.android.ichooseforyou.model;

/**
 * Created by jians on 1/22/15.
 */
public class Item {
    private String content;
    private boolean isVisible;

    public Item(String content) {
        this.content = content;
        isVisible = false;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setVisible(boolean visible) {
        this.isVisible = visible;
    }

    public boolean isVisible() {
        return isVisible;
    }

}
